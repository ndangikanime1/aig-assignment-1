### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ d888a903-a7e4-49fe-87a8-410fd632f4cd
md"# Create an enum for the network domain"

# ╔═╡ 79eddd30-af48-11eb-33b3-2787a72cbd10
 @enum NetworkDomain one two three four

# ╔═╡ 8bd6e0c3-b68b-4f49-96c4-6130c9f66063
md"## Initialise variables to types"

# ╔═╡ d02ef168-e3b9-4649-b711-813faccecdd8
mutable struct CSPVariable
name::String
value::Union{Nothing,NetworkDomain}
restricted_values::Vector{NetworkDomain}
domain_restriction_number::Int64
end

# ╔═╡ b871644b-e602-4231-9e85-df55ab3d1479
struct CSPConstraints
var::Vector{CSPVariable}
constraints::Vector{Tuple{CSPVariable,CSPVariable}}
end

# ╔═╡ c0e35a1f-645d-477f-ac0e-930444772dc9
net = rand(setdiff(Set([one,two,three,four]), Set([one,four])))

# ╔═╡ 92a45a6f-6f26-4e52-991c-0778967652cb
md"## Function to solve the CSP Implementation"

# ╔═╡ 3d3a2633-3986-408c-8d30-1d854c8e794d
function csp_solver(pb::CSPConstraints, all_val_assigns)
for current_variable in pb.var
if current_variable.domain_restriction_number == 4
return []
else
next_value = rand(setdiff(Set([one,two,three,four]),
Set(current_variable.restricted_values)))
#assign the value to the variable
current_variable.value = next_value
#forward motion
for current_constraint in pb.constraints
if !((current_constraint[1] == current_variable) || (current_constraint[2] ==
current_variable))
continue
else
if current_constraint[1] == current_variable
push!(current_constraint[2].restricted_values, next_value)
current_constraint[2].domain_restriction_number += 1
# when the number reached is maximum then go backwards
# else continue moving forward 
else
push!(current_constraint[1].restricted_values, next_value)
current_constraint[1].domain_restriction_number += 1
# else continue moving forward 
						end
end
end
# add the assignment to all_val_assigns
push!(all_val_assigns, current_variable.name => next_value)
end
end
return all_val_assigns
end


# ╔═╡ be708339-d806-4e53-a366-cf47d8fa2946
md"## Randomly assigning each network domain to a set of values"

# ╔═╡ 40cdf009-88bb-48ac-b651-2dfc4660f04f
md"## Assigning Variables to Constraints"

# ╔═╡ f7af5cd0-1ba2-4bc8-b14a-b607f417a7b7
x1 = CSPVariable("X1",nothing, [], 0)

# ╔═╡ 1df166e8-a375-4063-87a7-45d10fc71703
x2 = CSPVariable("X2", nothing, [], 0)

# ╔═╡ cf2d5a3c-ee73-46bb-8204-d1902a8f23f4
x3 = CSPVariable("X3", nothing, [], 0)


# ╔═╡ 836dd282-98f9-4f7c-96f5-2bfe6888ce51
x4 = CSPVariable("X4", nothing, [], 0)


# ╔═╡ 72d10ecd-79f2-41a9-8664-b63f72d5dda8
x5 = CSPVariable("X5", nothing, [], 0)

# ╔═╡ 055a9f23-0c84-4181-9535-c5076cb7bd1b
x6 = CSPVariable("X6", nothing, [], 0)

# ╔═╡ 949c5bef-f835-48de-8d0c-6feb4d0cfa82
x7 = CSPVariable("X7", nothing, [], 0)

# ╔═╡ e7d11cd8-1650-440e-8aa6-b93bb467765b
md"## Initialisng Domains with Variables"

# ╔═╡ 1ec76c10-121b-4e7b-aace-216ad95f19dc
solution = CSPConstraints([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])

# ╔═╡ 4e248270-c536-4031-8905-cf585a2aebf1
begin
	colors = ["1","2","3","4"]
	states = ["X1","X2","X3","X4","X5","X6","X7"]  
	solutions = ["X7: 1", "X6: 2", "X5: 3", "X4: 3", "X3: 1", "X2: 1", "X1: 4"]
end

# ╔═╡ 6ae7a776-ceca-473a-90b3-912f5104a9e7
csp_solver(solution, []),solutions

# ╔═╡ a5a8f134-b835-4f90-965c-e6df08b5ff3f


# ╔═╡ Cell order:
# ╟─d888a903-a7e4-49fe-87a8-410fd632f4cd
# ╠═79eddd30-af48-11eb-33b3-2787a72cbd10
# ╠═8bd6e0c3-b68b-4f49-96c4-6130c9f66063
# ╠═d02ef168-e3b9-4649-b711-813faccecdd8
# ╠═b871644b-e602-4231-9e85-df55ab3d1479
# ╠═c0e35a1f-645d-477f-ac0e-930444772dc9
# ╟─92a45a6f-6f26-4e52-991c-0778967652cb
# ╠═3d3a2633-3986-408c-8d30-1d854c8e794d
# ╠═be708339-d806-4e53-a366-cf47d8fa2946
# ╠═40cdf009-88bb-48ac-b651-2dfc4660f04f
# ╠═f7af5cd0-1ba2-4bc8-b14a-b607f417a7b7
# ╠═1df166e8-a375-4063-87a7-45d10fc71703
# ╠═cf2d5a3c-ee73-46bb-8204-d1902a8f23f4
# ╠═836dd282-98f9-4f7c-96f5-2bfe6888ce51
# ╠═72d10ecd-79f2-41a9-8664-b63f72d5dda8
# ╠═055a9f23-0c84-4181-9535-c5076cb7bd1b
# ╠═949c5bef-f835-48de-8d0c-6feb4d0cfa82
# ╠═e7d11cd8-1650-440e-8aa6-b93bb467765b
# ╠═1ec76c10-121b-4e7b-aace-216ad95f19dc
# ╠═4e248270-c536-4031-8905-cf585a2aebf1
# ╠═6ae7a776-ceca-473a-90b3-912f5104a9e7
# ╠═a5a8f134-b835-4f90-965c-e6df08b5ff3f
